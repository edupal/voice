Morris.Bar({
    element: 'graph-bar',
    data: [
        {x: 'Very Effective', y: 9, z: 2, a: 3 , b:3},
        {x: 'Somewhat Effective', y: 2, z: null, a: 1, b:2},
        {x: 'Neutral', y: 6, z: 2, a: 4, b:1},
        {x: 'Not Very Effective', y: 2, z: 4, a: 3, b: 2}
    ],
    xkey: 'x',
    ykeys: ['y', 'z', 'a', 'b'],
    labels: ['Freshman', 'Sophmores', 'Juniors', 'Seniors'],
    barColors:['#E67A77','#D9DD81','#79D1CF','#95BBD7']


});



var day_data = [
    {"elapsed": "I", "value": 34},
    {"elapsed": "II", "value": 24},
    {"elapsed": "III", "value": 3},
    {"elapsed": "IV", "value": 12},
    {"elapsed": "V", "value": 13},
    {"elapsed": "VI", "value": 22},
    {"elapsed": "VII", "value": 5},
    {"elapsed": "VIII", "value": 26},
    {"elapsed": "IX", "value": 12},
    {"elapsed": "X", "value": 19}
];
Morris.Line({
    element: 'graph-line',
    data: day_data,
    xkey: 'elapsed',
    ykeys: ['value'],
    labels: ['value'],
    lineColors:['#1FB5AD'],
    parseTime: false
});

// Use Morris.Area instead of Morris.Line





