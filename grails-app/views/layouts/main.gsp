<!DOCTYPE HTML>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	<asset:link rel="shortcut icon" href="avatar.png" type="image/x-icon"/>

	<style>
	section {
		padding-bottom: 0 !important;
	}
	</style>
	<asset:stylesheet src="application.css" />

	<g:layoutHead/>
</head>
<body>
<nav class="navbar navbar-default" style="border-radius:0; border:0;">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>

			<a class="navbar-brand" href="http://www.edupal.co/voice/umbc">
				<asset:image src="voice.png" class="logo" />
			</a>

		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>		<section id="container">
	<section id="main-content">
		<section class="wrapper" style="display:inline-block;">

			<g:layoutBody/>

		</section>
	</section>
</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<asset:javascript src="application.js" />

<script>
	$( document ).ready(function() {
		$( ".dropdown a" ).click(function() {
			var course = $(this).closest("a").text();
			$("#course").val(course);
		});
	});

</script>
</body>
</html>